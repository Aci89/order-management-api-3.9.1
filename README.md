# Order Management API RAML specification

This RAML specification is developed to manage and maintain orders.
Please find the addressed use cases below:
1. A consumer may periodically (every 5 minutes) consume the API to enable it (the consumer) to maintain a copy of the provider API's orders (the API represents the system of record)

2. A mobile application used by customer service representatives that uses the API to retrieve and update the order details

3. Simple extension of the API to support future resources such as customers and products

# Mulesoft implementation
Please find the Mulesoft test implementation on GitLab at:
https://gitlab.com/Aci89/order-management-api-3.9.1/

To run the API application please deploy on Mule 3.9.1 runtime using the below 'username' and password for 'Basic Auth':
* username: `Admin`
* password: `Admin`

# NOTE
Due to technical reasons the Mulesoft implementation was developed in Mule 3.9.1 runtime and Anypoint Studio 6.5.0

# Security configurations
##### HTTPS
The project was developed under the assumption that servers are secured using HTTPS (TLS1.2) to prevent attacks such as, but not limited to, 'Man in the middle attacks', 'Data sniffing' and 'Eavesdropping'.
##### TLS
Using TLS to exchange keys for authentication, allows a set of APIs and its users to guarantee message integrity when interfacing with consumers or systems.
#####  VPN
Ideally the MuleESB server would be located within a VPN using controlled and restricted access. This serves as an extra layer of security to protect the data and allow access only to authorized clients/consumers.

##### Basic Auth
Basic Auth, combined with HTTPS to provide confidentiality, can be a simple security measure to implement when protecting access to API endpoints.
It allows for a user/consumer to provide 'username' and 'password' to authenticate with the API and have access to the various endpoints.
However this is more suitable for internal systems.

##### OAuth2.0
OAuth2.0 is considered a more robust method of authorising access to resources since credentials are not traveling over the network at every request, unlike basic auth. Although it is a more secure way, OAuth2.0 is not an authentication method, since it does not authenticate the consumer/user but only authorizes it based on credentials provided.

##### Authorization vs Authentication
OAuth2.0 provides authorization, since it grants a consumer access to a resource based on credentials provided. That is all the API will know about that request, the API will not be able to authenticate the user making the request in any way (API will not be able to know who made the request or from which system the request was made from).

Authentication can be achieved by combining an 'Authentication platform' and OAuth2.0 for 'Authorization'. The consumer that wants to request access to the 'Order Management API' will have to register the client (mobile device or any client that requires external Authentication) to a platform. 

Once the user has been successfully authenticated (login) it can be provided with OAuth2.0 credentials to the API and can finally be authorized to make requests to the endpoints.
## API RAML design
##### 1/2. Requesting 'Orders' periodically / Mobile application usage
To satisfy use case 1, the consumer will make a request to the `/v1/orders?pageID=1` end point to retrieve orders.
The API will make use of **pagination** to avoid high network volume, high system loads and reduce data redundancy. The consumer will provide a *query parameter* specifying what page they would like back.

To retrieve orders periodically (every 5 minutes for example), the consumer will be provided with a JSON property that specifies the time of the last order created and each subsequent request. The consumer will provide this property back to the API so that only the newest orders will be returned, avoiding redundancy in the data and reducing load on the system behind the API (SQL database for example).

The API will respond with an '*x*' number of orders at a time. It will also provide a total count of orders in the database and the pointer to the next element for the next request (plus a number of details useful for the consumer client/frontend).

The response will look like the following:
```json
{
   "orders":[
      {
         "orderID":"001",
         "purchaseOrder":"Work Order",
         "products":[
            {
               "productID":"p1",
               "quantity":"5",
               "productName":"Macbook",
               "productPrice":"$1000.00"
            }
         ],
         "customers":[
            {
               "customerID":"c001",
               "customerName":"Ross",
               "customerLastname":"Fichera",
               "customerDateOfBirth":"23/06/1989"
            }
         ]
      },
      {
         "orderID":"002",
         "purchaseOrder":"Work Order",
         "products":[
            {
               "productID":"p2",
               "quantity":"2",
               "productName":"Mouse",
               "productPrice":"$50.00"
            }
         ],
         "customers":[
            {
               "customerID":"c002",
               "customerName":"Dina",
               "customerLastname":"Gouda",
               "customerDateOfBirth":"19/09/1994"
            }
         ]
      },
      {
         "orderID":"003",
         "purchaseOrder":"Private Order",
         "products":[
            {
               "productID":"p4",
               "quantity":"2",
               "productName":"Monitor",
               "productPrice":"$200.00"
            }
         ],
         "customers":[
            {
               "customerID":"c003",
               "customerName":"Peter",
               "customerLastname":"Smith",
               "customerDateOfBirth":"03/03/1987"
            }
         ]
      }
   ],
   "_metadata":{
      "page":1,
      "per_page":3,
      "page_count":3,
      "total_count":100,
      "last_udpated":"1549425276588",
      "next":"/v1/orders?pageID=2"
   }
}
```
The mobile applications that have access to the 'Order Management API' will also be able to create, update and delete orders using the following endpoints:

**Create**
`POST: /v1/orders/`
A post will create a new order given that the correct body is provided, like below:
```json
{
  "purchaseOrderID": "PO001",
  "products": [
    {
      "productID":"p1",
      "quantity":"5"
    }
  ],
  "customerIDs":[
    "c001"
  ]
}
```
If the request is successful, the API will respond with the below message:
```json
{
	"statusResponse": {
		"status": "OK",
		"message": "Success"
	}
}
```
**Update**
`PUT: /v1/orders/${orderID}`
Update will allow the consumer to update an existing order using the below body in the PUT request:
```json
{
  "purchaseOrderID": "PO001",
  "products": [
    {
      "productID":"p1",
      "quantity":"5"
    }
  ],
  "customerIDs":[
    "c001"
  ]
}
```
In this case we are providing a `URI` parameter to specify the ID of the resource to modify/update. If the request does not succeed, for example the resource could not be found, the below error will be returned:
```json
{
  "errorResponse": {
    "errorCode": "404",
    "errorSeverity": "ERROR",
    "errorDescription": "Resource not found",
    "errorServer": "MuleSoft",
    "errorTechnicalDetails": "(org.mule.module.apikit.exception.NotFoundException)"
  }
}
```
**Delete**
`DELETE: /v1/orders/${orderID}`
Deleting a resource can be achieved by making a request  to the API specifying a ``URI`` parameter that indicates the resource to delete. Again when successful, the API with respond with a success message.
```json
{
	"statusResponse": {
		"status": "OK",
		"message": "Success"
	}
}
```
Otherwise an error will be returned, depending on the occurrence. For example if the consumer provides the wrong **Basic Auth** credentials.
```json
{
  "errorResponse": {
    "errorCode": "403",
    "errorSeverity": "ERROR",
    "errorDescription": "Access Forbidden - You do not have permission to access this resource",
    "errorServer": "MuleSoft",
    "errorTechnicalDetails": "Could not find required GrantedAuthority for principal \"IT_SUPPORT\". Access denied. (org.mule.api.security.NotPermittedException)"
  }
}
```
##### 3. Extending the use of the API
Further down the line the API might be expanded to use more or new resources that are now available to the system.
Given the principles of re-usability applied to the ``RAML`` design, it is possible to expand the API for other resources that weren't originally part of the implementation. 
``ResourceTypes`` and ``CollectionType`` allow the API to be easily extended to new resources and collections.
The error exception strategy chosen also allows to reuse the error messaging to an extended number of scenarios and can be quite easily extended to allow for more.

## Design choices
##### Exception strategy design choices
The error strategies chosen for the API are split into 2 categories:
* Messaging errors
* System errors

Messaging errors are errors that occur within the Mulesoft flow and are strictly related to Mule application running (validation errors and expression errors for example).
System errors are errors that happen at a system level (database connections or network related errors for example), where the Mule flow is executing but the system behind it is returning an error code.
The 'Order Management API' was developed separating these concepts into 2 different error strategies.
##### Messaging exception strategy
This exception strategy will handle `API Kit Router Errors`
```xml
<apikit:mapping-exception-strategy name="order-management-apiKitGlobalExceptionMapping">
        <apikit:mapping statusCode="404">
            <apikit:exception value="org.mule.module.apikit.exception.NotFoundException" />
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="#[getResource('src/main/api/json/examples/errorResponses/GenericErrorResponse_400.json').asString()]" doc:name="Set Payload" mimeType="application/json"/>
        </apikit:mapping>
        <apikit:mapping statusCode="405">
            <apikit:exception value="org.mule.module.apikit.exception.MethodNotAllowedException" />
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="{ &quot;message&quot;: &quot;Method not allowed&quot; }" doc:name="Set Payload"/>
        </apikit:mapping>
        <apikit:mapping statusCode="415">
            <apikit:exception value="org.mule.module.apikit.exception.UnsupportedMediaTypeException" />
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="{ &quot;message&quot;: &quot;Unsupported media type&quot; }" doc:name="Set Payload"/>
        </apikit:mapping>
        <apikit:mapping statusCode="406">
            <apikit:exception value="org.mule.module.apikit.exception.NotAcceptableException" />
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="{ &quot;message&quot;: &quot;Not acceptable&quot; }" doc:name="Set Payload"/>
        </apikit:mapping>
        <apikit:mapping statusCode="400">
            <apikit:exception value="org.mule.module.apikit.exception.BadRequestException" />
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="{ &quot;message&quot;: &quot;Bad request&quot; }" doc:name="Set Payload"/>
        </apikit:mapping>
    </apikit:mapping-exception-strategy>
```
##### System exception strategy
This strategy will handle the errors coming from the systems used.
```xml
<choice-exception-strategy name="order-managementChoice_Exception_Strategy">
        <catch-exception-strategy doc:name="400 Errors" when="#[message.outboundProperties['http.status']  &gt; 300]">
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="{   &quot;errorResponse&quot;: {     &quot;errorCode&quot;: &quot;#[message.outboundProperties['http.status']]&quot;,     &quot;errorSeverity&quot;: &quot;ERROR&quot;,      &quot;errorServer&quot;: &quot;MuleSoft&quot;,     &quot;errorTechnicalDetails&quot;: &quot;#[(exception.cause!=null)?(java.net.URLEncoder.encode(exception.cause.message,'UTF-8')):(java.net.URLEncoder.encode(exception, 'UTF-8'))]&quot;   } }" mimeType="application/json" doc:name="Set Payload"/>
        </catch-exception-strategy>
        <catch-exception-strategy when="#[message.inboundProperties['http.status']  == null]" doc:name="500 Errors">
            <set-variable variableName="httpStatus" value="500" doc:name="Set http status"/>
            <message-properties-transformer doc:name="Message Properties">
                <add-message-property key="http.status" value="#[flowVars.httpStatus]"/>
            </message-properties-transformer>
            <set-property propertyName="Content-Type" value="application/json" doc:name="Property"/>
            <set-payload value="{   &quot;errorResponse&quot;: {     &quot;errorCode&quot;: &quot;#[flowVars.httpStatus]&quot;,     &quot;errorSeverity&quot;: &quot;ERROR&quot;,      &quot;errorServer&quot;: &quot;MuleSoft&quot;,     &quot;errorTechnicalDetails&quot;: &quot;#[(exception.cause!=null)?(java.net.URLEncoder.encode(exception.cause.message,'UTF-8')):(java.net.URLEncoder.encode(exception, 'UTF-8'))]&quot;   } }" mimeType="application/json" doc:name="Set Payload"/>
        </catch-exception-strategy>
    </choice-exception-strategy>
```
The `Choice Exception strategy` is very useful to abstract the error exception strategy and renders it portable and re-usable for various errors across systems.

The combination of the 2 strategies allows the API to cover edge and exceptional scenarios that might occur during the lifetime of the API.

##### API layers design
For the sake of the optional task I have developed an API (Order Management API) which is a stand alone Mule application which, if connected to real systems, would connect to a back-end and perform operations on it.

By industry standards, this is not the recommended approach.
The three layered approach recommended by Mulesoft is the following:
* `System API` (order-management-system-api)
    * System APIs connect to the core systems, like databases and back ends, and retrieve raw data from them. The system API does not alter the data in any way, but maintains a high state of re-usability by multiple clients. 
    * Ideally you would have a 1 to 1 relationship between the system API and systems in your organization.
* `Process API` (customer-process-api | order-process-api | product-process-api | order-management-process-api)
    * Process APIs allow you to define a common process which the organization can share. It serves as the orchestration layer where data gets collated from many process APIs.
    * Process APIs still maintain a high level of re-usability across the organization.
* `Experience API` (tonys-shop-experience-api | reps-mobile-experience-api)
    * And finally, the Experience APIs are the means by which data can be reconfigured so that it is most easily consumed by its intended audience, all from a common data source. The bulk of the data transformation happens in the experience API.
    * This layer is tightly coupled with the front-end/consumer of the API and the level of re-usability is reduced.
##### Domain project
Last but not least, the `Domain project`.
Again for the sake of the optional task, there was no need for a domain project since I only used one API to showcase some of the practices in API development.

The domain project is used to **share resources** across multiple APIs.
Some examples as per Mulesoft documentation ([Shared Resources](https://docs.mulesoft.com/mule-runtime/4.1/shared-resources)):
* Expose multiple services within the domain through the same port.
* Share the connection to persistent storage.
* Share services between apps through a well-defined interface.
* Ensure consistency between apps upon any changes, since the configuration is only set in one place.

The domain project can also be used for referencing secrets across APIs (**NOTE** secrets should not be stored within the domain but only referenced from it, secrets should be stored preferably outside the domain app).